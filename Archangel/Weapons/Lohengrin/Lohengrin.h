#pragma once
#include "Common.h"

#ifdef _MD_
	#pragma comment (lib, "lohengrin_MD.lib" )
#endif

#ifdef _MDd_
	#pragma comment (lib, "lohengrin_MDd.lib" )
#endif

#ifdef _MDo_
	#pragma comment (lib, "lohengrin_MDo.lib" )
#endif

#ifdef _MT_
	#pragma comment (lib, "lohengrin_MT.lib" )
#endif

#ifdef _MTd_
	#pragma comment (lib, "lohengrin_MTd.lib" )
#endif

#ifdef _MTo_
	#pragma comment (lib, "lohengrin_MTo.lib" )
#endif

#pragma comment (lib, "version.lib" )

