#include "stdafx.h"

// Packet Encryption : 2007.07.13
const wchar_t* PACKET_VERSION_C = _T("10.02.30.8"); // Login, switch, client
const wchar_t* PACKET_VERSION_S = _T("10.04.06.8"); // Other servers
