#ifndef WEAPON_AILEPACK_AILEPACK_H
#define WEAPON_AILEPACK_AILEPACK_H

#ifdef _MD_
#pragma comment (lib, "AilePack_MD.lib" )
#endif

#ifdef _MDd_
#pragma comment (lib, "AilePack_MDd.lib" )
#endif

#ifdef _MDo_
#pragma comment (lib, "AilePack_MDo.lib" )
#endif

#ifdef _MT_
#pragma comment (lib, "AilePack_MT.lib" )
#endif

#ifdef _MTd_
#pragma comment (lib, "AilePack_MTd.lib" )
#endif

#ifdef _MTo_
#pragma comment (lib, "AilePack_MTo.lib" )
#endif


#endif // WEAPON_AILEPACK_AILEPACK_H