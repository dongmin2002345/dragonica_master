#ifndef WEAPON_HELLDART_HELLDART_H
#define WEAPON_HELLDART_HELLDART_H

#ifdef _MD_
	#pragma comment (lib, "helldart_MD.lib" )
#endif

#ifdef _MDd_
	#pragma comment (lib, "helldart_MDd.lib" )
#endif

#ifdef _MDo_
	#pragma comment (lib, "helldart_MDo.lib" )
#endif

#ifdef _MT_
	#pragma comment (lib, "helldart_MT.lib" )
#endif

#ifdef _MTd_
	#pragma comment (lib, "helldart_MTd.lib" )
#endif

#ifdef _MTo_
	#pragma comment (lib, "helldart_MTo.lib" )
#endif

#endif // WEAPON_HELLDART_HELLDART_H