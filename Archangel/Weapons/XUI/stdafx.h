#ifndef WEAPON_XUI_STDAFX_H
#define WEAPON_XUI_STDAFX_H

#define WIN32_LEAN_AND_MEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#include <xutility>
#include <iostream>
#include <tchar.h>
#include <windows.h>

#include "mmsystem.h"

#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <list>

#include "BM/GUID.h"
#include "BM/STLsupport.h"
#include "BM/ClassSupport.h"
#include "BM/vstring.h"
#include "BM/STLSupport.h"

#include "CSIME/Common.h"


//
#include "XUI_Include.h"

#endif // WEAPON_XUI_STDAFX_H