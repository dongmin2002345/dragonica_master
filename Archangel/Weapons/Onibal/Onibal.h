#pragma once
#include "lwOnibal.h"

#ifdef _MD_
	#pragma comment (lib, "Onibal_MD.lib" )
	#pragma comment (lib, "ACE_vc8_Static_MD.lib")
	#pragma comment (lib, "lua_tinker_MD.lib")
	#pragma comment (lib, "lua_lib_MD.lib")
#endif

#ifdef _MDd_
	#pragma comment (lib, "Onibal_MDd.lib" )
	#pragma comment (lib, "ACE_vc8_Static_MDd.lib")
	#pragma comment (lib, "lua_tinker_MDd.lib")
	#pragma comment (lib, "lua_lib_MDd.lib")
#endif

#ifdef _MDo_
	#pragma comment (lib, "Onibal_MDo.lib" )
	#pragma comment (lib, "ACE_vc8_Static_MDo.lib")
	#pragma comment (lib, "lua_tinker_MDo.lib")
	#pragma comment (lib, "lua_lib_MDo.lib")
#endif

#ifdef _MT_
	#pragma comment (lib, "Onibal_MT.lib" )
	#pragma comment (lib, "ACE_vc8_Static_MT.lib")
	#pragma comment (lib, "lua_tinker_MT.lib")
	#pragma comment (lib, "lua_lib_MT.lib")
#endif

#ifdef _MTd_
	#pragma comment (lib, "Onibal_MTd.lib" )
	#pragma comment (lib, "ACE_vc8_Static_MTd.lib")
	#pragma comment (lib, "lua_tinker_MTd.lib")
	#pragma comment (lib, "lua_lib_MTd.lib")
#endif

#ifdef _MTo_
	#pragma comment (lib, "Onibal_MTo.lib" )
	#pragma comment (lib, "ACE_vc8_Static_MTo.lib")
	#pragma comment (lib, "lua_tinker_MTo.lib")
	#pragma comment (lib, "lua_lib_MTo.lib")
#endif