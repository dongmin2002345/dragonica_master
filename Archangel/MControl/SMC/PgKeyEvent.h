#ifndef MACHINE_SMC_PGKEYEVENT_H
#define MACHINE_SMC_PGKEYEVENT_H

extern bool RegistKeyEvent();
extern bool CALLBACK OnTerminateServer(WORD const& rkInputKey);

#endif // MACHINE_SMC_PGKEYEVENT_H