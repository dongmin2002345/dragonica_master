#ifndef MACHNIE_MMC_NETWORK_PGTIMER_H
#define MACHNIE_MMC_NETWORK_PGTIMER_H

extern void CALLBACK Timer_Connector(DWORD dwUserData);
extern void CALLBACK Timer_ServerState(DWORD dwUserData);

#endif // MACHNIE_MMC_NETWORK_PGTIMER_H