#ifndef __DEBUG_STUB__
#define __DEBUG_STUB__

void debug_init(lua_State* L);
void debug_write(const char* szText);

#endif