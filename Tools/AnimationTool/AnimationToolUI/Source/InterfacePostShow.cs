using System;

namespace AnimationToolUI
{
	/// <summary>
	/// Summary description for InterfacePostShow.
	/// </summary>
    public interface IPostShow
    {
        void PostShow();
    }
}
