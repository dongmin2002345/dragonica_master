//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by QuestChecker.rc
//
#define IDI_MAINFRAME                   102
#define IDD_QUESTCHECKER                103
#define IDD_TEXTVIEW                    104
#define IDR_CHECKMENU                   105
#define IDD_LOGWINDOW                   106
#define IDC_TXT_DEVFOLDER               1002
#define IDC_BTN_DEVCHANGE               1003
#define IDC_TXT_QUESTID                 1004
#define IDC_BTN_VIEWTEXTLIST            1005
#define IDC_TXT_QUESTNAME               1006
#define IDC_TXT_QUESTFINDER             1007
#define IDC_CHK_QPTS_OPTION             1008
#define IDC_BTN_SIMULATION              1009
#define IDC_BTN_START                   1010
#define IDC_TXT_PORT                    1011
#define IDC_TXT_LOGINID                 1012
#define IDC_TXT_LOGINPW                 1013
#define IDC_LIST_QUESTTABLE             1014
#define IDC_IPADDRESS                   1015
#define IDC_STATUS                      1016
#define IDC_BTN_QUEST_PTS2              1017
#define IDC_BTN_QUEST_EMPTY             1017
#define IDC_LIST_QUESTERROR             1018
#define IDC_LIST_TEXTTABLE              1019
#define IDC_LIST_QUEST_EMPTY            1019
#define IDC_BUTTON1                     1020
#define IDC_LIST_QUESTERROR2            1020
#define IDC_LIST_DBNOTEXIST             1020
#define IDC_LIST_LOG                    1021
#define IDC_SELECTTEXT                  1022
#define IDC_LIST_ERROR                  1022
#define IDC_RDO_ALL                     1024
#define IDC_RDO_ERROR                   1025
#define IDC_RDO_FAIL                    1026
#define IDC_LIST_LOG2                   1027
#define IDC_RADIO_DB                    1031
#define IDC_RADIO_LOCAL                 1032
#define IDC_MAX_KILLCOUNT_EDIT          1034
#define IDC_LIST1                       1035
#define IDC_BUTTON2                     1037
#define IDC_BUILD_BAG_RATE              1037
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_40003                        40003
#define ID_40004                        40004
#define ID_40005                        40005
#define ID_40006                        40006
#define ID_40007                        40007
#define ID_40008                        40008
#define ID_40009                        40009
#define ID_40010                        40010
#define ID_FOLDERSELECT                 40011
#define ID_OUTPUT                       40012
#define ID_EXIT                         40013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40014
#define _APS_NEXT_CONTROL_VALUE         1039
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
