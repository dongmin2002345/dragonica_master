﻿namespace Emergent.Gamebryo.SceneDesigner.GUI.ProjectG
{
	partial class MonsterBagForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_lbMonsterList = new System.Windows.Forms.ListBox();
			this.m_tbBagID = new System.Windows.Forms.TextBox();
			this.m_tbMon1ID = new System.Windows.Forms.TextBox();
			this.m_tbMon1Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon2Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon2ID = new System.Windows.Forms.TextBox();
			this.m_tbMon3Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon3ID = new System.Windows.Forms.TextBox();
			this.m_tbMon4Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon4ID = new System.Windows.Forms.TextBox();
			this.m_tbMon5Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon5ID = new System.Windows.Forms.TextBox();
			this.m_tbMon10Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon10ID = new System.Windows.Forms.TextBox();
			this.m_tbMon9Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon9ID = new System.Windows.Forms.TextBox();
			this.m_tbMon8Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon8ID = new System.Windows.Forms.TextBox();
			this.m_tbMon7Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon7ID = new System.Windows.Forms.TextBox();
			this.m_tbMon6Rate = new System.Windows.Forms.TextBox();
			this.m_tbMon6ID = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.m_tbMemo = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.m_lblMonName1 = new System.Windows.Forms.Label();
			this.m_lblMonName2 = new System.Windows.Forms.Label();
			this.m_lblMonName3 = new System.Windows.Forms.Label();
			this.m_lblMonName4 = new System.Windows.Forms.Label();
			this.m_lblMonName5 = new System.Windows.Forms.Label();
			this.m_lblMonName10 = new System.Windows.Forms.Label();
			this.m_lblMonName9 = new System.Windows.Forms.Label();
			this.m_lblMonName8 = new System.Windows.Forms.Label();
			this.m_lblMonName7 = new System.Windows.Forms.Label();
			this.m_lblMonName6 = new System.Windows.Forms.Label();
			this.m_btnOK = new System.Windows.Forms.Button();
			this.m_btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// m_lbMonsterList
			// 
			this.m_lbMonsterList.FormattingEnabled = true;
			this.m_lbMonsterList.HorizontalScrollbar = true;
			this.m_lbMonsterList.ItemHeight = 12;
			this.m_lbMonsterList.Location = new System.Drawing.Point(458, 84);
			this.m_lbMonsterList.Name = "m_lbMonsterList";
			this.m_lbMonsterList.ScrollAlwaysVisible = true;
			this.m_lbMonsterList.Size = new System.Drawing.Size(217, 136);
			this.m_lbMonsterList.TabIndex = 0;
			// 
			// m_tbBagID
			// 
			this.m_tbBagID.Location = new System.Drawing.Point(102, 12);
			this.m_tbBagID.Name = "m_tbBagID";
			this.m_tbBagID.Size = new System.Drawing.Size(100, 21);
			this.m_tbBagID.TabIndex = 1;
			// 
			// m_tbMon1ID
			// 
			this.m_tbMon1ID.Location = new System.Drawing.Point(12, 84);
			this.m_tbMon1ID.Name = "m_tbMon1ID";
			this.m_tbMon1ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon1ID.TabIndex = 3;
			// 
			// m_tbMon1Rate
			// 
			this.m_tbMon1Rate.Location = new System.Drawing.Point(59, 84);
			this.m_tbMon1Rate.Name = "m_tbMon1Rate";
			this.m_tbMon1Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon1Rate.TabIndex = 4;
			// 
			// m_tbMon2Rate
			// 
			this.m_tbMon2Rate.Location = new System.Drawing.Point(59, 111);
			this.m_tbMon2Rate.Name = "m_tbMon2Rate";
			this.m_tbMon2Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon2Rate.TabIndex = 6;
			// 
			// m_tbMon2ID
			// 
			this.m_tbMon2ID.Location = new System.Drawing.Point(12, 111);
			this.m_tbMon2ID.Name = "m_tbMon2ID";
			this.m_tbMon2ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon2ID.TabIndex = 5;
			// 
			// m_tbMon3Rate
			// 
			this.m_tbMon3Rate.Location = new System.Drawing.Point(59, 138);
			this.m_tbMon3Rate.Name = "m_tbMon3Rate";
			this.m_tbMon3Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon3Rate.TabIndex = 8;
			// 
			// m_tbMon3ID
			// 
			this.m_tbMon3ID.Location = new System.Drawing.Point(12, 138);
			this.m_tbMon3ID.Name = "m_tbMon3ID";
			this.m_tbMon3ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon3ID.TabIndex = 7;
			// 
			// m_tbMon4Rate
			// 
			this.m_tbMon4Rate.Location = new System.Drawing.Point(59, 165);
			this.m_tbMon4Rate.Name = "m_tbMon4Rate";
			this.m_tbMon4Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon4Rate.TabIndex = 10;
			// 
			// m_tbMon4ID
			// 
			this.m_tbMon4ID.Location = new System.Drawing.Point(12, 165);
			this.m_tbMon4ID.Name = "m_tbMon4ID";
			this.m_tbMon4ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon4ID.TabIndex = 9;
			// 
			// m_tbMon5Rate
			// 
			this.m_tbMon5Rate.Location = new System.Drawing.Point(59, 192);
			this.m_tbMon5Rate.Name = "m_tbMon5Rate";
			this.m_tbMon5Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon5Rate.TabIndex = 12;
			// 
			// m_tbMon5ID
			// 
			this.m_tbMon5ID.Location = new System.Drawing.Point(12, 192);
			this.m_tbMon5ID.Name = "m_tbMon5ID";
			this.m_tbMon5ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon5ID.TabIndex = 11;
			// 
			// m_tbMon10Rate
			// 
			this.m_tbMon10Rate.Location = new System.Drawing.Point(283, 192);
			this.m_tbMon10Rate.Name = "m_tbMon10Rate";
			this.m_tbMon10Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon10Rate.TabIndex = 22;
			// 
			// m_tbMon10ID
			// 
			this.m_tbMon10ID.Location = new System.Drawing.Point(236, 192);
			this.m_tbMon10ID.Name = "m_tbMon10ID";
			this.m_tbMon10ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon10ID.TabIndex = 21;
			// 
			// m_tbMon9Rate
			// 
			this.m_tbMon9Rate.Location = new System.Drawing.Point(283, 165);
			this.m_tbMon9Rate.Name = "m_tbMon9Rate";
			this.m_tbMon9Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon9Rate.TabIndex = 20;
			// 
			// m_tbMon9ID
			// 
			this.m_tbMon9ID.Location = new System.Drawing.Point(236, 165);
			this.m_tbMon9ID.Name = "m_tbMon9ID";
			this.m_tbMon9ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon9ID.TabIndex = 19;
			// 
			// m_tbMon8Rate
			// 
			this.m_tbMon8Rate.Location = new System.Drawing.Point(283, 138);
			this.m_tbMon8Rate.Name = "m_tbMon8Rate";
			this.m_tbMon8Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon8Rate.TabIndex = 18;
			// 
			// m_tbMon8ID
			// 
			this.m_tbMon8ID.Location = new System.Drawing.Point(236, 138);
			this.m_tbMon8ID.Name = "m_tbMon8ID";
			this.m_tbMon8ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon8ID.TabIndex = 17;
			// 
			// m_tbMon7Rate
			// 
			this.m_tbMon7Rate.Location = new System.Drawing.Point(283, 111);
			this.m_tbMon7Rate.Name = "m_tbMon7Rate";
			this.m_tbMon7Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon7Rate.TabIndex = 16;
			// 
			// m_tbMon7ID
			// 
			this.m_tbMon7ID.Location = new System.Drawing.Point(236, 111);
			this.m_tbMon7ID.Name = "m_tbMon7ID";
			this.m_tbMon7ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon7ID.TabIndex = 15;
			// 
			// m_tbMon6Rate
			// 
			this.m_tbMon6Rate.Location = new System.Drawing.Point(283, 84);
			this.m_tbMon6Rate.Name = "m_tbMon6Rate";
			this.m_tbMon6Rate.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon6Rate.TabIndex = 14;
			// 
			// m_tbMon6ID
			// 
			this.m_tbMon6ID.Location = new System.Drawing.Point(236, 84);
			this.m_tbMon6ID.Name = "m_tbMon6ID";
			this.m_tbMon6ID.Size = new System.Drawing.Size(41, 21);
			this.m_tbMon6ID.TabIndex = 13;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(84, 12);
			this.label1.TabIndex = 22;
			this.label1.Text = "MonsterBagID";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(234, 15);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 12);
			this.label2.TabIndex = 24;
			this.label2.Text = "Memo";
			// 
			// m_tbMemo
			// 
			this.m_tbMemo.Location = new System.Drawing.Point(281, 12);
			this.m_tbMemo.Name = "m_tbMemo";
			this.m_tbMemo.Size = new System.Drawing.Size(100, 21);
			this.m_tbMemo.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(10, 66);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(16, 12);
			this.label3.TabIndex = 25;
			this.label3.Text = "ID";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(57, 66);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(30, 12);
			this.label4.TabIndex = 26;
			this.label4.Text = "Rate";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(237, 66);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(16, 12);
			this.label5.TabIndex = 27;
			this.label5.Text = "ID";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(281, 66);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(30, 12);
			this.label6.TabIndex = 28;
			this.label6.Text = "Rate";
			// 
			// m_lblMonName1
			// 
			this.m_lblMonName1.AutoSize = true;
			this.m_lblMonName1.Location = new System.Drawing.Point(106, 87);
			this.m_lblMonName1.Name = "m_lblMonName1";
			this.m_lblMonName1.Size = new System.Drawing.Size(38, 12);
			this.m_lblMonName1.TabIndex = 29;
			this.m_lblMonName1.Text = "label7";
			// 
			// m_lblMonName2
			// 
			this.m_lblMonName2.AutoSize = true;
			this.m_lblMonName2.Location = new System.Drawing.Point(106, 114);
			this.m_lblMonName2.Name = "m_lblMonName2";
			this.m_lblMonName2.Size = new System.Drawing.Size(38, 12);
			this.m_lblMonName2.TabIndex = 30;
			this.m_lblMonName2.Text = "label8";
			// 
			// m_lblMonName3
			// 
			this.m_lblMonName3.AutoSize = true;
			this.m_lblMonName3.Location = new System.Drawing.Point(106, 141);
			this.m_lblMonName3.Name = "m_lblMonName3";
			this.m_lblMonName3.Size = new System.Drawing.Size(38, 12);
			this.m_lblMonName3.TabIndex = 31;
			this.m_lblMonName3.Text = "label9";
			// 
			// m_lblMonName4
			// 
			this.m_lblMonName4.AutoSize = true;
			this.m_lblMonName4.Location = new System.Drawing.Point(106, 168);
			this.m_lblMonName4.Name = "m_lblMonName4";
			this.m_lblMonName4.Size = new System.Drawing.Size(44, 12);
			this.m_lblMonName4.TabIndex = 32;
			this.m_lblMonName4.Text = "label10";
			// 
			// m_lblMonName5
			// 
			this.m_lblMonName5.AutoSize = true;
			this.m_lblMonName5.Location = new System.Drawing.Point(106, 194);
			this.m_lblMonName5.Name = "m_lblMonName5";
			this.m_lblMonName5.Size = new System.Drawing.Size(44, 12);
			this.m_lblMonName5.TabIndex = 33;
			this.m_lblMonName5.Text = "label11";
			// 
			// m_lblMonName10
			// 
			this.m_lblMonName10.AutoSize = true;
			this.m_lblMonName10.Location = new System.Drawing.Point(330, 194);
			this.m_lblMonName10.Name = "m_lblMonName10";
			this.m_lblMonName10.Size = new System.Drawing.Size(44, 12);
			this.m_lblMonName10.TabIndex = 38;
			this.m_lblMonName10.Text = "label12";
			// 
			// m_lblMonName9
			// 
			this.m_lblMonName9.AutoSize = true;
			this.m_lblMonName9.Location = new System.Drawing.Point(330, 168);
			this.m_lblMonName9.Name = "m_lblMonName9";
			this.m_lblMonName9.Size = new System.Drawing.Size(44, 12);
			this.m_lblMonName9.TabIndex = 37;
			this.m_lblMonName9.Text = "label13";
			// 
			// m_lblMonName8
			// 
			this.m_lblMonName8.AutoSize = true;
			this.m_lblMonName8.Location = new System.Drawing.Point(330, 141);
			this.m_lblMonName8.Name = "m_lblMonName8";
			this.m_lblMonName8.Size = new System.Drawing.Size(44, 12);
			this.m_lblMonName8.TabIndex = 36;
			this.m_lblMonName8.Text = "label14";
			// 
			// m_lblMonName7
			// 
			this.m_lblMonName7.AutoSize = true;
			this.m_lblMonName7.Location = new System.Drawing.Point(330, 114);
			this.m_lblMonName7.Name = "m_lblMonName7";
			this.m_lblMonName7.Size = new System.Drawing.Size(44, 12);
			this.m_lblMonName7.TabIndex = 35;
			this.m_lblMonName7.Text = "label15";
			// 
			// m_lblMonName6
			// 
			this.m_lblMonName6.AutoSize = true;
			this.m_lblMonName6.Location = new System.Drawing.Point(330, 87);
			this.m_lblMonName6.Name = "m_lblMonName6";
			this.m_lblMonName6.Size = new System.Drawing.Size(44, 12);
			this.m_lblMonName6.TabIndex = 34;
			this.m_lblMonName6.Text = "label16";
			// 
			// m_btnOK
			// 
			this.m_btnOK.Location = new System.Drawing.Point(519, 238);
			this.m_btnOK.Name = "m_btnOK";
			this.m_btnOK.Size = new System.Drawing.Size(75, 23);
			this.m_btnOK.TabIndex = 39;
			this.m_btnOK.Text = "OK";
			this.m_btnOK.UseVisualStyleBackColor = true;
			this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
			// 
			// m_btnCancel
			// 
			this.m_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.m_btnCancel.Location = new System.Drawing.Point(600, 238);
			this.m_btnCancel.Name = "m_btnCancel";
			this.m_btnCancel.Size = new System.Drawing.Size(75, 23);
			this.m_btnCancel.TabIndex = 40;
			this.m_btnCancel.Text = "Cancel";
			this.m_btnCancel.UseVisualStyleBackColor = true;
			// 
			// MonsterBagForm
			// 
			this.AcceptButton = this.m_btnOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.m_btnCancel;
			this.ClientSize = new System.Drawing.Size(687, 273);
			this.Controls.Add(this.m_btnCancel);
			this.Controls.Add(this.m_btnOK);
			this.Controls.Add(this.m_lblMonName10);
			this.Controls.Add(this.m_lblMonName9);
			this.Controls.Add(this.m_lblMonName8);
			this.Controls.Add(this.m_lblMonName7);
			this.Controls.Add(this.m_lblMonName6);
			this.Controls.Add(this.m_lblMonName5);
			this.Controls.Add(this.m_lblMonName4);
			this.Controls.Add(this.m_lblMonName3);
			this.Controls.Add(this.m_lblMonName2);
			this.Controls.Add(this.m_lblMonName1);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.m_tbMemo);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.m_tbMon10Rate);
			this.Controls.Add(this.m_tbMon10ID);
			this.Controls.Add(this.m_tbMon9Rate);
			this.Controls.Add(this.m_tbMon9ID);
			this.Controls.Add(this.m_tbMon8Rate);
			this.Controls.Add(this.m_tbMon8ID);
			this.Controls.Add(this.m_tbMon7Rate);
			this.Controls.Add(this.m_tbMon7ID);
			this.Controls.Add(this.m_tbMon6Rate);
			this.Controls.Add(this.m_tbMon6ID);
			this.Controls.Add(this.m_tbMon5Rate);
			this.Controls.Add(this.m_tbMon5ID);
			this.Controls.Add(this.m_tbMon4Rate);
			this.Controls.Add(this.m_tbMon4ID);
			this.Controls.Add(this.m_tbMon3Rate);
			this.Controls.Add(this.m_tbMon3ID);
			this.Controls.Add(this.m_tbMon2Rate);
			this.Controls.Add(this.m_tbMon2ID);
			this.Controls.Add(this.m_tbMon1Rate);
			this.Controls.Add(this.m_tbMon1ID);
			this.Controls.Add(this.m_tbBagID);
			this.Controls.Add(this.m_lbMonsterList);
			this.Name = "MonsterBagForm";
			this.Text = "MonsterBagForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox m_lbMonsterList;
		private System.Windows.Forms.TextBox m_tbBagID;
		private System.Windows.Forms.TextBox m_tbMon1ID;
		private System.Windows.Forms.TextBox m_tbMon1Rate;
		private System.Windows.Forms.TextBox m_tbMon2Rate;
		private System.Windows.Forms.TextBox m_tbMon2ID;
		private System.Windows.Forms.TextBox m_tbMon3Rate;
		private System.Windows.Forms.TextBox m_tbMon3ID;
		private System.Windows.Forms.TextBox m_tbMon4Rate;
		private System.Windows.Forms.TextBox m_tbMon4ID;
		private System.Windows.Forms.TextBox m_tbMon5Rate;
		private System.Windows.Forms.TextBox m_tbMon5ID;
		private System.Windows.Forms.TextBox m_tbMon10Rate;
		private System.Windows.Forms.TextBox m_tbMon10ID;
		private System.Windows.Forms.TextBox m_tbMon9Rate;
		private System.Windows.Forms.TextBox m_tbMon9ID;
		private System.Windows.Forms.TextBox m_tbMon8Rate;
		private System.Windows.Forms.TextBox m_tbMon8ID;
		private System.Windows.Forms.TextBox m_tbMon7Rate;
		private System.Windows.Forms.TextBox m_tbMon7ID;
		private System.Windows.Forms.TextBox m_tbMon6Rate;
		private System.Windows.Forms.TextBox m_tbMon6ID;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox m_tbMemo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label m_lblMonName1;
		private System.Windows.Forms.Label m_lblMonName2;
		private System.Windows.Forms.Label m_lblMonName3;
		private System.Windows.Forms.Label m_lblMonName4;
		private System.Windows.Forms.Label m_lblMonName5;
		private System.Windows.Forms.Label m_lblMonName10;
		private System.Windows.Forms.Label m_lblMonName9;
		private System.Windows.Forms.Label m_lblMonName8;
		private System.Windows.Forms.Label m_lblMonName7;
		private System.Windows.Forms.Label m_lblMonName6;
		private System.Windows.Forms.Button m_btnOK;
		private System.Windows.Forms.Button m_btnCancel;
	}
}