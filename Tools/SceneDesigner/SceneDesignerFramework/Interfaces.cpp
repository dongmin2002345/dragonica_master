// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2006 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

// This file includes all interface header files. Since these files
// are just interfaces and have no associated source files, they must be
// included here to ensure that the interfaces are part of the assembly.

#include "ICommand.h"
#include "ICommandPanel.h"
#include "IInteractionMode.h"
#include "IPlugin.h"
#include "IRenderingMode.h"
#include "IService.h"
