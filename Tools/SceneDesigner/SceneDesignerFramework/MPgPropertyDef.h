#pragma once

extern const char* gs_pcPGProperty[];
extern int gs_iPGPropertyCount;
extern const char* gs_pcPGAlphaGroup[];
extern int gs_iPGAlphaGroupCount;
extern const char* gs_pcPGOptimization[];
extern int gs_iPGOptimizationCount;

// Component
// Animation Object
extern const char* gs_pcPropAnimationObject;
extern const char* gs_pcPropPhase;

// Scene Graph
extern const char* gs_pcPropSceneGraph;
extern const char* gs_pcNIFFilePath;

extern const char* gs_pcHideObject;
