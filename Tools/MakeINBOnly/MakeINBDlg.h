// MakeINBDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CMakeINBDlg dialog
class CMakeINBDlg : public CDialog
{
// Construction
public:
	CMakeINBDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MAKEINB_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnConvToInb();
	afx_msg void OnBnClickedDir();
	afx_msg void OnEnChangeName();
	CEdit m_strNameEdit;
};
