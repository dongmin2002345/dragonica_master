#include	"defines.h"

const std::wstring WSTR_NULLSTRING	= L"";
const std::wstring WSTR_XMLPATH		= L"./xml/path.xml";
const std::wstring WSTR_XMLEFFECT	= L"./xml/effect.xml";

const std::wstring WSTR_FOLDERPATH_XML		= L"XML";
const std::wstring WSTR_FOLDERPATH_GSA		= L"GSA";
const std::wstring WSTR_FOLDERPATH_NIF		= L"NIF";

const std::wstring WSTR_SYSTEM_LISTCOLUMN = L"@Message Log|990";
const std::wstring WSTR_LOG_LISTCOLUMN = L"@Success Log|702@State|290";
const std::wstring WSTR_LOG_LISTITEM = L"%s@%s";
const std::wstring WSTR_ERRORLOG_LISTCOLUMN = L"@Error Log|702@State|290";
const std::wstring WSTR_ERRORLOG_LISTITEM = L"%s@%s";
std::wstring const WSTR_CONFIG_FILE = L"./WorldChecker.ini";
std::wstring const WSTR_TABLE_FILE = L"./Table/";


const TCHAR *ComboBOX_iTem[] = {
	TEXT("DefMap"), 
	TEXT("effect.xml"),
};

