#pragma once
#include "randomlib/Random.hpp"

#ifdef _MT_
	#pragma comment(lib, "randomlib_MT.lib")
#endif

#ifdef _MTd_
	#pragma comment(lib, "randomlib_MTd.lib")
#endif

#ifdef _MTo_
	#pragma comment(lib, "randomlib_MTo.lib")
#endif

#ifdef _MD_
	#pragma comment(lib, "randomlib_MD.lib")
#endif

#ifdef _MDd_
	#pragma comment(lib, "randomlib_MDd.lib")
#endif

#ifdef _MDo_
	#pragma comment(lib, "randomlib_MDo.lib")
#endif