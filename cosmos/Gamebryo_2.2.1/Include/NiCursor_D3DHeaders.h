// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2006 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef NICURSOR_D3DHEADERS_H
#define NICURSOR_D3DHEADERS_H

#include <NiDirect3DVersion.h>

#if defined(_USRDLL)
    #if defined(_DX9)
        #define NIDX9RENDERER_IMPORT
    #endif  //
#endif  //#if defined(_USRDLL)

#include <NiD3DRendererHeaders.h>

#endif  //#ifndef NICURSOR_D3DHEADERS_H
