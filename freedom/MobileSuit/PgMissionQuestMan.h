#ifndef FREEDOM_DRAGONICA_CONTENTS_MISSIONQUEST_PGMISSIONQUESTMAN_H
#define FREEDOM_DRAGONICA_CONTENTS_MISSIONQUEST_PGMISSIONQUESTMAN_H

//#include "Variant/PgMissionQuest.h"
//
//class PgMissionQuestMan : public PgMissionQuestMng
//{
//protected:
//	typedef	std::map< int, std::wstring >	IllustPathContainer;
//	IllustPathContainer		m_MissionIllust;
//	IllustPathContainer		m_MissionBg;
//
//protected:
//	bool ParseXml(TiXmlNode const* pkNode);
//	int GetMissionKey(int const iItemNo) const;
//
//public:
//	bool Parse(std::wstring const &wstrFileName);
//	std::wstring const GetMQCardIllustDirToCardID(int const iCardID) const;
//	std::wstring const GetMQCardIllustDirToQuestID(int const iQuestID) const;
//	std::wstring const GetMQCardGradeBg(int const iGrade) const;
//
//	PgMissionQuestMan();
//	~PgMissionQuestMan();
//};
//
//#define g_kMissionQuestMng (Loki::SingletonHolder<PgMissionQuestMan>::Instance())

#endif // FREEDOM_DRAGONICA_CONTENTS_MISSIONQUEST_PGMISSIONQUESTMAN_H