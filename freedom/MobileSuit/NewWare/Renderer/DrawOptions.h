
// ****************************************************************************************
//
//     Project : Dragonica Optimize and Refactoring
//   Copyright : Copyright (C) 2009 Barunson Interactive, Inc
//        Name : DrawOptions.h
// Description : .
//      Author : Jae-Ryoung, Lee
//
// Remarks :
//   * .
// 
// Revisions :
//  09/09/30 LeeJR First Created
//

#ifndef _RENDERER_DRAWOPTIONS_H__
#define _RENDERER_DRAWOPTIONS_H__


namespace NewWare
{

namespace Renderer
{


namespace DrawOptions
{
    float const FRUSTUM_FAR_DEF_LANGE = 40000.0f;
} //namespace DrawOptions


} //namespace Renderer

} //namespace NewWare


#endif //_RENDERER_DRAWOPTIONS_H__
