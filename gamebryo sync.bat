xcopy /Y /R ..\Core\cosmos_src\include\GameBryo\SDK\win32\include\*.h .\cosmos\GameBryo_2.2.2\include\*.h

xcopy /Y /R ..\Core\cosmos_src\include\GameBryo\SDK\win32\include\*.inl .\cosmos\GameBryo_2.2.2\include\*.inl

xcopy /Y /S ..\Core\cosmos_src\include\GameBryo\SDK\win32\lib\VC80\*.lib .\cosmos\GameBryo_2.2.2\lib\*.lib

xcopy /Y /S ..\Core\cosmos_src\include\GameBryo\SDK\win32\lib\VC80\*.pdb .\cosmos\GameBryo_2.2.2\lib\*.pdb
